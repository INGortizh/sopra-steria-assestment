import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import PrivateRoute from './components/privateRoute';
import Home from './routes/home/Home';
import UsersList from './routes/userList/UserList';
import ViewUser from './routes/viewUser/ViewUser';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route
          path="users"
          element={
            <PrivateRoute>
              <UsersList />
            </PrivateRoute>
          }
        ></Route>
        <Route
          path="view/user/:userId"
          element={
            <PrivateRoute>
              <ViewUser />
            </PrivateRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
