import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import Wrapper from './test.utils';

test('renders learn react link', () => {
  const app = render(
    <Wrapper>
      <App />
    </Wrapper>
  );
  expect(app).toMatchSnapshot();
});
