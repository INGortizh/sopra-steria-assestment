import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from '@redux-saga/core';
import usersReducer from './reducers/users';
import sessionReducer from './reducers/session';
import fetchStatusReducer from './reducers/fetchStatus';
import mySaga from './sagas';

const sagaMidelWare = createSagaMiddleware();
const rootReducer = combineReducers({
  usersReducer,
  sessionReducer,
  fetchStatusReducer
});
const store = createStore(rootReducer, applyMiddleware(sagaMidelWare));
sagaMidelWare.run(mySaga);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
