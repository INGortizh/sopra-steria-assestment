import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router';

const PrivateRoute = ({ children }) => {
  const { isLoggedIn } = useSelector((state) => state.sessionReducer);
  if (isLoggedIn) {
    return <>{children}</>;
  } else {
    return <Navigate to="/" />;
  }
};

PrivateRoute.propTypes = {
  children: PropTypes.element
};

export default PrivateRoute;
