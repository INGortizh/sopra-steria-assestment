import React from 'react';
import PropTypes from 'prop-types';
import SpinnerContainer from './Styles';
import SpinnerWrapper from './SpinnerWrapper';
import { Spinner } from 'reactstrap';

const AppSpinner = ({ showSpinner }) => {
  if (showSpinner) {
    return (
      <SpinnerWrapper>
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      </SpinnerWrapper>
    );
  }
  return null;
};

AppSpinner.propTypes = {
  showSpinner: PropTypes.bool.isRequired
};

export default AppSpinner;
