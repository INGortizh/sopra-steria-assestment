import React from 'react';
import { render } from '@testing-library/react';
import AppSpinner from './Spinner';

describe('Spinner Component', () => {
  it('match snapshot', () => {
    const spinner = render(<AppSpinner showSpinner={true} />);
    expect(spinner).toMatchSnapshot();
  });
});
