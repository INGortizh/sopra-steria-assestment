import styled from 'styled-components';

const SpinnerContainer = styled.div`
  position: absolute;
  width: 80vw;
  height: 80vh;
  display: flex;
  justify-content: center;
  border-radius: 20px;
  background-color: rgba(0, 0, 0, 0.4);
  margin: 0 auto;
  left: 50vw;
  top: 50vh;
  transform: translate(-50%, -50%);
  z-index: 1000;
  & div {
    position: relative;
    top: 50%;
    border-color: white transparent;
  }
`;

export default SpinnerContainer;
