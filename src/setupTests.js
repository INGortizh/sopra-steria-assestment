// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
/* eslint react/prop-types: 0 */
/* eslint react/display-name: 0 */
/* eslint no-undef: 0 */
import React from 'react';
import '@testing-library/jest-dom';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useNavigate: jest.fn(),
  useParams: () => ({ userId: 1 })
}));

jest.mock('./components/spinner/SpinnerWrapper', () => () => {
  const SpinnerWrapperMock = ({ children }) => <div>{children}</div>;
  return <SpinnerWrapperMock />;
});

global.fetch = jest.fn();

beforeEach(() => {
  fetch.mockReset();
});
