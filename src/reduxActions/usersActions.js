export const GET_USERS_FETCH = 'GET_USERS_FETCH';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const FETCH_A_USER = 'FETCH_A_USER';
export const FETCH_A_USER_SUCCESS = 'FETCH_A_USER_SUCCESS';
export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';

export const getUsersFetch = (id) => ({
  type: GET_USERS_FETCH,
  payload: id
});

export const fetchAUser = (id) => ({
  type: FETCH_A_USER,
  payload: id
});

export const updateUser = (data) => ({
  type: UPDATE_USER,
  payload: data
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: id
});
