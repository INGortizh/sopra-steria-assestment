export const IS_LOADING = 'IS_LOADING';
export const IS_NOT_LOADING = 'IS_NOT_LOADING';

export const setIsLoading = () => ({
  type: IS_LOADING
});

export const setIsNotLoading = () => ({
  type: IS_NOT_LOADING
});
