export const LOG_USER_IN = 'LOG_USER_IN';
export const SUCCESS_LOG_IN = 'SUCCESS_LOG_IN';
export const FAILED_LOG_IN = 'FAILED_LOG_IN';

export const logUserIn = (email, password) => ({
  type: LOG_USER_IN,
  payload: { email, password }
});
