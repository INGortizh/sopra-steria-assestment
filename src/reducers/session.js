import { SUCCESS_LOG_IN, FAILED_LOG_IN } from '../reduxActions/sessionActions';

const defaultState = {
  isLoggedIn: false
};

const sessionReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SUCCESS_LOG_IN:
      return { ...state, isLoggedIn: true };
    case FAILED_LOG_IN:
      return { ...state, isLoggedIn: false };
    default:
      return state;
  }
};

export default sessionReducer;
