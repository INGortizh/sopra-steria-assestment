import {
  FETCH_A_USER_SUCCESS,
  GET_USERS_SUCCESS,
  UPDATE_USER_SUCCESS,
  DELETE_USER_SUCCESS
} from '../reduxActions/usersActions';
const defaultState = {
  usersList: [],
  selectedUser: {}
};

const usersReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_USERS_SUCCESS:
      return { ...state, usersList: action.users };
    case FETCH_A_USER_SUCCESS:
      return { ...state, selectedUser: action.payload };
    case UPDATE_USER_SUCCESS: {
      const newUser = action.payload;
      const nextUserList = state.usersList.map((user) =>
        user.id === newUser.id ? newUser : user
      );
      return { ...state, selectedUser: newUser, usersList: nextUserList };
    }
    case DELETE_USER_SUCCESS: {
      console.log(action.payload);
      const id = action.payload;
      const nextUserList = state.usersList.filter((user) => user.id !== id);
      return { ...state, usersList: nextUserList };
    }
    default:
      return state;
  }
};

export default usersReducer;
