import { IS_LOADING, IS_NOT_LOADING } from '../reduxActions/fetchStatusActions';
const defaultState = {
  isLoading: false
};

const fetchStatusReducer = (state = defaultState, action) => {
  switch (action.type) {
    case IS_LOADING:
      return { ...state, isLoading: true };
    case IS_NOT_LOADING:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default fetchStatusReducer;
