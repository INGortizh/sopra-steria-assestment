import React from 'react';
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from '@redux-saga/core';
import usersReducer from './reducers/users';
import sessionReducer from './reducers/session';
import fetchStatusReducer from './reducers/fetchStatus';
import mySaga from './sagas';

const sagaMidelWare = createSagaMiddleware();
const rootReducer = combineReducers({
  usersReducer,
  sessionReducer,
  fetchStatusReducer
});
const store = createStore(rootReducer, applyMiddleware(sagaMidelWare));
sagaMidelWare.run(mySaga);
/* eslint react/prop-types: 0 */
function Wrapper({ children }) {
  return <Provider store={store}>{children}</Provider>;
}

export default Wrapper;
