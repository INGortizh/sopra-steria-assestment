import { takeEvery, call, put } from 'redux-saga/effects';
import {
  FAILED_LOG_IN,
  LOG_USER_IN,
  SUCCESS_LOG_IN
} from '../reduxActions/sessionActions';
import {
  FETCH_A_USER,
  FETCH_A_USER_SUCCESS,
  GET_USERS_FETCH,
  GET_USERS_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  DELETE_USER,
  DELETE_USER_SUCCESS
} from '../reduxActions/usersActions';
import {
  setIsLoading,
  setIsNotLoading
} from '../reduxActions/fetchStatusActions';

function* fetchHelper(callback) {
  yield put(setIsLoading());
  yield callback();
  yield put(setIsNotLoading());
}

function usersFetch() {
  return fetch('https://reqres.in/api/users')
    .then((res) => {
      return res.json();
    })
    .then(({ data }) => data);
}

async function singleUserFetch(id) {
  return fetch(`https://reqres.in/api/users/${id}`)
    .then((res) => {
      return res.json();
    })
    .then(({ data }) => data);
}

async function updateUserFetch(data) {
  return fetch(`https://reqres.in/api/users/${data.id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
    .then((res) => {
      return res.json();
    })
    .catch(() => ({}));
}

async function deleteUserFetch(id) {
  return fetch(`https://reqres.in/api/users/${id}`, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' }
  })
    .then((res) => {
      if (res.status === 204) {
        return res.json();
      } else {
        return new Error('failed to delete user');
      }
    })
    .catch(() => ({}));
}

async function logUserInFetch({ email, password }) {
  try {
    const data = await fetch('https://reqres.in/api/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password
      })
    });
    return data.json();
  } catch (error) {
    console.log(error);
  }
}

function* workGetUsersFetch() {
  yield fetchHelper(function* () {
    const users = yield call(usersFetch);
    yield put({ type: GET_USERS_SUCCESS, users });
  });
}

function* workFetchAUser(action) {
  yield fetchHelper(function* () {
    const data = yield call(singleUserFetch, action.payload);
    yield put({ type: FETCH_A_USER_SUCCESS, payload: data ?? {} });
  });
}

function* workLogUserIn(action) {
  yield fetchHelper(function* () {
    const data = yield call(logUserInFetch, action.payload);
    if (Object.keys(data).find((key) => key === 'token')) {
      yield put({ type: SUCCESS_LOG_IN });
    } else {
      yield put({ type: FAILED_LOG_IN });
    }
  });
}

function* workUpdateUser(action) {
  yield fetchHelper(function* () {
    const data = yield call(updateUserFetch, action.payload);
    const user = { ...data };
    delete user.updatedAt;
    delete user.createdAt;
    yield put({ type: UPDATE_USER_SUCCESS, payload: user });
  });
}

function* workDeleteUser(action) {
  yield fetchHelper(function* () {
    yield call(deleteUserFetch, action.payload);
    yield put({ type: DELETE_USER_SUCCESS, payload: action.payload });
  });
}

function* mySaga() {
  yield takeEvery(GET_USERS_FETCH, workGetUsersFetch);
  yield takeEvery(FETCH_A_USER, workFetchAUser);
  yield takeEvery(LOG_USER_IN, workLogUserIn);
  yield takeEvery(UPDATE_USER, workUpdateUser);
  yield takeEvery(DELETE_USER, workDeleteUser);
}

export default mySaga;
