import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Wrapper from '../../test.utils';
import ViewUser from './ViewUser';

describe('ViewUser test', () => {
  beforeEach(() => {
    fetch.mockResolvedValue(
      new Promise((resolve) =>
        resolve({
          json: () =>
            new Promise((resolveJson) =>
              resolveJson({
                data: {
                  id: 1,
                  email: 'george.bluth@reqres.in',
                  first_name: 'George',
                  last_name: 'Bluth',
                  avatar: 'https://reqres.in/img/faces/1-image.jpg'
                }
              })
            )
        })
      )
    );
  });
  it('edit user details', async () => {
    const viewUser = render(
      <Wrapper>
        <ViewUser />
      </Wrapper>
    );

    const { findByTestId, getByDisplayValue } = viewUser;
    const editUserButton = await findByTestId('edit-user-button');
    const nameField = getByDisplayValue('George');
    expect(nameField).toBeDisabled();
    fireEvent.click(editUserButton);
    expect(nameField).not.toBeDisabled();
    fireEvent.change(nameField, { target: { value: 'George Harrison' } });
    expect(nameField.value).toBe('George Harrison');
  });
  it('match snapshot', () => {
    const viewUser = render(
      <Wrapper>
        <ViewUser />
      </Wrapper>
    );
    expect(viewUser).toMatchSnapshot();
  });
});
