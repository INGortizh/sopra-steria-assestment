import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAUser, updateUser } from '../../reduxActions/usersActions';
import { useNavigate } from 'react-router';
import * as Styled from './Styled';
import AppSpinner from '../../components/spinner/Spinner';
import { Button } from 'reactstrap';
import { Input } from 'reactstrap';

const defaultFormData = {
  avatar: '',
  email: '',
  first_name: '',
  id: 0,
  last_name: ''
};

const ViewUser = () => {
  const { userId } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    usersReducer: { selectedUser },
    fetchStatusReducer: { isLoading }
  } = useSelector((state) => state);
  useEffect(() => {
    dispatch(fetchAUser(userId));
  }, []);
  const [formData, setFormData] = useState(defaultFormData);
  const [isFormInactive, setIsFormInactive] = useState(true);
  const [isSaveDisabled, setIsSaveDisabled] = useState(true);
  useEffect(() => {
    setFormData(selectedUser);
  }, [selectedUser]);

  const handleInput = ({ target }) => {
    const { name, value } = target;
    setFormData({ ...formData, [name]: value });
    setIsSaveDisabled(false);
  };

  const handleSave = () => {
    dispatch(updateUser(formData));
  };

  const { avatar, email, first_name, last_name } = formData;
  return (
    <Styled.MainContainer>
      <AppSpinner showSpinner={isLoading} />
      {formData.id ? (
        <Styled.VerticalContainer>
          <Styled.UserCard>
            <img src={avatar} />
            <Styled.VerticalContainer>
              <Styled.Section>
                <Styled.SubTitle>Name:</Styled.SubTitle>
                <Input
                  name="first_name"
                  value={first_name}
                  disabled={isFormInactive}
                  onChange={handleInput}
                />
              </Styled.Section>
              <Styled.Section>
                <Styled.SubTitle>Last Name:</Styled.SubTitle>
                <Input
                  name="last_name"
                  value={last_name}
                  disabled={isFormInactive}
                  onChange={handleInput}
                />
              </Styled.Section>
              <Styled.Section>
                <Styled.SubTitle>Email:</Styled.SubTitle>
                <Input
                  name="email"
                  value={email}
                  disabled={isFormInactive}
                  onChange={handleInput}
                />
              </Styled.Section>
            </Styled.VerticalContainer>
          </Styled.UserCard>
          <Styled.ButtonContainer>
            <Button onClick={() => navigate('/users')}>Go Back</Button>
            <Button
              onClick={() => setIsFormInactive(false)}
              data-testid="edit-user-button"
            >
              Edit
            </Button>
            <Button
              onClick={handleSave}
              disabled={isSaveDisabled}
              data-testid="save-user-button"
            >
              Save
            </Button>
          </Styled.ButtonContainer>
        </Styled.VerticalContainer>
      ) : (
        <Styled.VerticalContainer>
          <span> not user found</span>
          <Button onClick={() => navigate('/users')}>Go Back</Button>
        </Styled.VerticalContainer>
      )}
    </Styled.MainContainer>
  );
};

export default ViewUser;
