import styled from 'styled-components';

const MainContainer = styled.div`
  width: 100vw;
  position: absolute;
  display: flex;
  justify-content: center;
  top: 30vh;
  transform: translate(0, -50%);
`;
const UserCard = styled.div`
  padding: 40px;
  display: flex;
  border: 2px solid #666;
  border-radius: 20px;
  & > *:first-child {
    margin-right: 20px;
  }
`;
const VerticalContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const SubTitle = styled.h3`
  font-size: 16px;
  margin: 0;
  font-weight: bold;
`;

const Section = styled.section`
  display: flex;
  flex-direction: column;
`;

const ButtonContainer = styled.div`
  margin-top: 20px;
  padding: 0 40px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

export {
  MainContainer,
  UserCard,
  VerticalContainer,
  SubTitle,
  Section,
  ButtonContainer
};
