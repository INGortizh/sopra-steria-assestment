import styled from 'styled-components';

const FormContainer = styled.div`
  position: relative;
  width: 40vw;
  margin: 0 auto;
  top: 40vh;
  transform: translate(0, -50%);
  border: solid 2px #666;
  border-radius: 20px;
  padding: 40px 40px 50px 40px;
  & button {
    margin-top: 15px;
    position: absolute;
    left: 50%;
    transform: translate(-50%);
  }
  @media (max-width: 450px) {
    width: 80vw;
  }
`;

export default FormContainer;
