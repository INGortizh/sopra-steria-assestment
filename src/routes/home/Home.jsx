import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { logUserIn } from '../../reduxActions/sessionActions';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import FormContainer from './Styles';
import AppSpinner from '../../components/spinner/Spinner';

const Home = () => {
  const [formData, setFormData] = useState({ email: '', password: '' });
  const dispatch = useDispatch();
  const {
    sessionReducer: { isLoggedIn },
    fetchStatusReducer: { isLoading }
  } = useSelector((state) => state);
  const { email, password } = formData;
  const navigate = useNavigate();
  useEffect(() => {
    if (isLoggedIn) navigate('/users');
  }, [isLoggedIn]);

  const handleInput = ({ target }) => {
    const { name, value } = target;
    setFormData({ ...formData, [name]: value });
  };

  const sendForm = (e) => {
    e.preventDefault();
    const { email, password } = formData;
    dispatch(logUserIn(email, password));
  };
  return (
    <>
      <FormContainer>
        <Form onSubmit={sendForm}>
          <FormGroup>
            <Label for="email-field">Email</Label>
            <Input
              name="email"
              value={email}
              id="email-field"
              onChange={handleInput}
            />
            <Label for="password-field">Password</Label>
            <Input
              name="password"
              value={password}
              id="password-field"
              onChange={handleInput}
            />
            <Button>Log In</Button>
          </FormGroup>
        </Form>
      </FormContainer>
      <AppSpinner showSpinner={isLoading} />
    </>
  );
};

export default Home;
