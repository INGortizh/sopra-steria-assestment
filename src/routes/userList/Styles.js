import styled from 'styled-components';

const StyledList = styled.dl`
  width: 80vw;
  margin: 0 auto;

  & > * {
    border-style: solid;
    border-color: #666;
    padding: 10px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
  }
  & > dd:last-child {
    border-bottom: 1px;
    border-style: solid;
    border-color: #666;
  }
`;
const ListHeader = styled.dt`
  border-bottom: 0;
`;
const ListItem = styled.dd`
  border-bottom: 0;
  margin: 0;
  & span {
    margin-right: 10px;
  }
`;

const ActionContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
`;

const MainContainer = styled.div`
  position: absolute;
  display: flex;
  width: 100vw;
  justify-content: center;
  top: 30vh;
  transform: translate(0, -50%);
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
export {
  StyledList,
  ListHeader,
  ListItem,
  ActionContainer,
  MainContainer,
  ButtonContainer
};
