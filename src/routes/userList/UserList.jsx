/* eslint-disable */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUsersFetch, deleteUser } from '../../reduxActions/usersActions';
import { useNavigate } from 'react-router';
import * as Styled from './Styles';
import { Button } from 'reactstrap';
import AppSpinner from '../../components/spinner/Spinner';

const UsersList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const {
    usersReducer: { usersList },
    fetchStatusReducer: { isLoading }
  } = useSelector((state) => state);

  const handleView = (id) => {
    navigate(`/view/user/${id}`);
  };
  const handleDelete = (id) => {
    dispatch(deleteUser(id));
  };

  return (
    <Styled.MainContainer>
      <AppSpinner showSpinner={isLoading} />
      {usersList.length ? (
        <Styled.StyledList>
          <Styled.ListHeader>
            <span>First Name</span>
            <span>Last Name</span>
            <span>Actions</span>
          </Styled.ListHeader>
          {usersList.map((user) => (
            <Styled.ListItem key={user.id}>
              <span>{user.first_name}</span>
              <span>{user.last_name}</span>
              <Styled.ActionContainer>
                <Button
                  outline
                  onClick={() => handleView(user.id)}
                  data-testid="view-user-button"
                >
                  view
                </Button>
                <Button
                  outline
                  onClick={() => handleDelete(user.id)}
                  data-testid="delete-user-button"
                >
                  delete
                </Button>
              </Styled.ActionContainer>
            </Styled.ListItem>
          ))}
        </Styled.StyledList>
      ) : (
        <Styled.ButtonContainer>
          <span>view all users by clicking the button bellow</span>
          <Button
            onClick={() => dispatch(getUsersFetch(1))}
            data-testid="get-users-button"
          >
            Get Users
          </Button>
        </Styled.ButtonContainer>
      )}
    </Styled.MainContainer>
  );
};

export default UsersList;
