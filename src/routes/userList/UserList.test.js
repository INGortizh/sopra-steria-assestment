import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Wrapper from '../../test.utils';
import UsersList from './UserList';

describe('UsersList test', () => {
  it('display list of users', () => {
    fetch.mockResolvedValue(
      new Promise((resolve) =>
        resolve({
          json: () =>
            new Promise((resolveJson) =>
              resolveJson({
                data: [
                  {
                    id: 1,
                    email: 'george.bluth@reqres.in',
                    first_name: 'George',
                    last_name: 'Bluth',
                    avatar: 'https://reqres.in/img/faces/1-image.jpg'
                  },
                  {
                    id: 2,
                    email: 'janet.weaver@reqres.in',
                    first_name: 'Janet',
                    last_name: 'Weaver',
                    avatar: 'https://reqres.in/img/faces/2-image.jpg'
                  },
                  {
                    id: 3,
                    email: 'emma.wong@reqres.in',
                    first_name: 'Emma',
                    last_name: 'Wong',
                    avatar: 'https://reqres.in/img/faces/3-image.jpg'
                  }
                ]
              })
            )
        })
      )
    );
    const usersList = render(
      <Wrapper>
        <UsersList />
      </Wrapper>
    );
    const { getByTestId } = usersList;
    const getUsersButton = getByTestId('get-users-button');
    fireEvent.click(getUsersButton);
    expect(fetch).toHaveBeenCalled();
  });
  it('match snapshot', () => {
    const usersList = render(
      <Wrapper>
        <UsersList />
      </Wrapper>
    );
    expect(usersList).toMatchSnapshot();
  });
});
